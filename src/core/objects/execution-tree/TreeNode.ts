import {CallArgs, CallInfo} from "../command/Command";

type BranchingInfo = (
  info: CallInfo,
  args: CallArgs,
  value: any,
  branches: { Left: (value: any) => Promise<any>, Right: (value: any) => Promise<any> }
) => void;

export class TreeNode {
  private left: TreeNode;
  private next: TreeNode;
  private right: TreeNode;

  private constructor(private fn: BranchingInfo) {
  }

  static create(fn: BranchingInfo): TreeNode {
    return new this(fn);
  }

  fork(left: TreeNode, right: TreeNode): this {
    this.left = left;
    this.right = right;

    return this;
  }

  then(next: TreeNode): this {
    this.next = next;

    return this;
  }

  async run(info: CallInfo, args: CallArgs, value: any): Promise<void> {
    // @ts-ignore
    const result = this.fn(info, args, await value, {
      Left: (value: any) => this.left.run(info, args, value),
      Right: (value: any) => this.right.run(info, args, value)
    });

    if (this.next) {
      this.next.run(info, args, await result);
    }
  }
}
