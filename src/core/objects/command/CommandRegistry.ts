import {CallInfo, Command} from "./Command";

export class CommandRegistry {
  protected registry: Map<string, Command>;

  constructor() {
    this.registry = new Map<string, Command>();
  }

  has(commandId: string): boolean {
    return this.registry.has(commandId);
  }

  run(commandId: string, info: CallInfo, strArgs?: string): void {
    if (this.registry.has(commandId)) {
      let instance = this.registry.get(commandId);

      return instance!.run(info, strArgs);
    }
  }

  register(command: Command): void {
    // Subcommands should not be registered as top-level commands.
    if (!command.isSubcommand()) {
      this.registry.set(command.name, command);
    }
  }
}
