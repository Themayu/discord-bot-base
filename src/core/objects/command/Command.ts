import {Message} from "discord.js";
import {CommandRegistry} from "./CommandRegistry";
import {ExecutionTree} from "../execution-tree/ExecutionTree";
import {DiscordConnection} from "../../bot";
import {isNil, parseStr} from "../../../lib/utils/language";

export type CallArgs = Map<string | symbol, string>;

export type CallInfo = DiscordConnection & {
  message: Message;
}

export class Command implements Command {
  protected subcommandRegistry: CommandRegistry = new CommandRegistry();

  constructor(
    public readonly name: string,
    public readonly procedure: ExecutionTree,
    protected readonly _parameters: string[] = [],
    protected readonly _parent?: Command) {
  }

  isSubcommand(): boolean {
    return !isNil(this._parent);
  }

  registerSubcommand(subcommand: Command): void {
    this.subcommandRegistry.register(subcommand);
  }

  run(info: CallInfo, strArgs?: string): void {
    if (typeof strArgs === "string") {
      let [subcommand, ...rest] = strArgs.split(" ");

      if (this.subcommandRegistry.has(subcommand)) {
        return this.subcommandRegistry.run(subcommand, info, rest.length ? rest.join(" ") : undefined);
      }
    }

    return this.procedure
      .run(info, parseStr(this._parameters, strArgs || ""), undefined);
  }

  get parent() {
    return this._parent;
  }
}
