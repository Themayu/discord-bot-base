import {OptionDocument} from "../../interfaces";
import {Model} from "mongoose";
import {isNil} from "../../../../../../lib/utils/language";

export async function setFn<T>(this: Model<OptionDocument>, option: string, value: T): Promise<T> {
  return this.findOneAndUpdate()
    .where("name").equals(option)
    .setUpdate({ $set: {value: isNil(value) ? null : value} })
    .setOptions({ upsert: true, new: true })
    .exec()
    .then(option => option!.value);
}
