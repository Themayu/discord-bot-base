export enum CacheValues {
  DISCORD_CONNECTION = "discordConnection",
  MONGOOSE_CONNECTION = "mongooseConnection"
}
